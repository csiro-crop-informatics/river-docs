#lang scribble/manual

@title{The Story Behind River}

I am including this section in the documentation purely because I believe
an understanding of the circumstances that led to the development of River
can increase understanding of what River is now, what it can become, and how
it can help you in your next project.

@local-table-of-contents{}

@section{The Problem}

There are a number of applications that have been built by CSIRO that help
researchers collect and access data. Each of these applications has its 
strengths and weaknesses. Some of the applications are unmaintained due
to funding. Many of these applications have kept on life support with 
a single maintainer on a small time allotment. As a project bounces around
between maintainers, domain specific logic is added to the project and the
maintainer becomes an expert in the application. As more features are added
complexity grows, hacks are deployed, and the app becomes hard to reason
about. It becomes difficult for new people to work on the project. 

To support the new Boorowa Agricultural Research Stationi, a team was created to 
build a single interface into a number of legacy applications. The goal
is to be able to connect data from each system and allow researchers to 
interact with multiple systems though a single interface. As per usual the
applications range from maintained and well documented to not maintained and
not documented. The applications also range drastically in complexity. 

Most of the applications share the problem that the complexity-of-software
to number-of-features ratio is quite high. That's just the way building software works.
Each time a new feature is added or tweaked, technical debt is accrued and generally
there simply is not enough money, people, or whatever required to manage the 
technical debt. 

Basically the problem for me was getting all of these systems, that thought 
about the world quite differently, to adapt their thinking to how the other systems
worked. For example, most systems used non globalized ids. This meant that 
two systems might contain information about the same experiment but have different
(or the same) ids about the experiment. Also, each system thought about access
and identity differently. Some systems allowed all users to access all data,
other systems had sophisticated logic to allow sets of users to access sets of
data. These differences made building an interface that combined information
from each system difficult. 

So, of course, I built translators between each application into a "master api"
that I used for this new interface. Well, actually, what I did was created another
dialect that the next person who comes along will have to learn. Sorry. 

Okay, so next time we build an application, what can we do differently?

@section{A Solution}

Well, as a programmer with a problem that involves integrations, my solution is to
abstract the differences between each system into something with strong communication
protocols. For example, we can build a generalized id protocol that each new application
could use that would generate globally unique ids and resolve each id to the application 
where the value lived. This would allow all applications to share ids freely. We could
also build an authorization service that these services could use. We could start building
a set of services that allowed each application to preform all domain unspecific tasks in
a complete controlled way. 

I think this solution would work, and I think there are teams at CSIRO doing this, but
it just seems like a lot of work. The idea we have is to make the abstraction at a different
level. The application. 

@section{A Different Solution}

Let's try and build an application that can be used to generate applications. I think
this would be everyone's first thought if the problem was not also so obvious: if all
applications have the same structure, how can we deal with domain specific logic. 

The response we have come up with is to move domain specific logic out of the application
and store it as data. 

@subsection{Is This a Good Idea}

I could see arguments going both ways, but the idea that has led me to believe this
is a viable solution is as follows. In the current model for how applications are built,
developers develop the application logic, but they also develop the domain specific
logic (ocasionally refered to as business logic). Developers generally don't know
anything about the domain specific language. Also the domain specific logic is often
viable to change fequently. So if we pulled the domain specific logic out of the 
application and stored it as data we get a number of benefits.

@itemlist[
  @item{Domain experts can work directly on the domain specific logic}
  @item{The application can respond to changes in domain specific logic without being rebuilt}
  @item{The application is only concerned with application logic}
  @item{Application procedures can be standardized across applications}
]

While at this point I am still sceptical about the overall viability of this idea, there is
enough evidence to make me believe this is worth trying. 

@section{The Early Days}

The idea is still fresh, the implementation is even newer. We are still coming up
with problems and solutions to this model frequently. Take what you read here
as what it is: speculative. This might work and be amazingly helpful. We might 
come across some problem that makes everything break. We are actively trying to 
come up with new problems that might underpin this solution for software development.
