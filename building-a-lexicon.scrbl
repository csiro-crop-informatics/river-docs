#lang scribble/manual

@require["definitions.rkt" scriblib/figure]

@title[#:tag "building-an-ontology"]{Building a River Application}

This section is about defining your ontology in River. This a 
required step before you are able to make measurements and observations
on your ontology.

@local-table-of-contents{}

@section{The ontology builder home screen}

This is the home screen for the ontology builder.

@define[ontology-dashboard "ontology-dashboard"]

@figure[ontology-dashboard "Ontology Dashboard"]{
  @image[ontology-home-marked]{Ontology Builder Image}
}

At any point you can navigate to this screen by clicking the wrench icon
in the navigation bar shown as feature 1 in @figure-ref["ontology-dashboard"].

You can open the `create a type' widget by clicking the create button.

You can also view a list of all the types you have created, illustrated by 
feature 3 in @figure-ref[ontology-dashboard].

@section{Creating a type}

To open the `create a type' widget click the create button on the ontology builder
dashboard. 

@define[ontology-create-widget "ontology-create-widget"]

@figure[ontology-create-widget "Creating a Type"]{
  @image[ontology-home-create]{Ontology Create a Type Image}
}

You can enter the name of the type you would like to create and click submit.
After this, your type will be added to the list of types in your ontology.

You can click on the type in order to navigate to the edit page.

@section{Editing A Type}

This documentation is coming soon.
