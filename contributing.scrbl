#lang scribble/manual

@require["definitions.rkt"]

@title{Where This Project Lives}

The aim of this section is to describe the components that make up
River as well as how you can access and use them. 

@local-table-of-contents{}

@section{A Demo Application}

There is a demo application running @link[river-app-url]{here}.
You can play around with a @bold{highly experimental} version of the application.

See Also:
@secref["building-an-ontology"],
@secref["using-river"],
@secref["getting-help"]

@section{Source Code}

The browser application is under @bold{very active development} and changing quite frequently.
For example the repository will be moving very soon. It is hosted
@link[river-repo]{in gitlab}.

See Also:
@secref["development"]

@section{Documentation}

If you are reading this you have probably already found documentation. In case not, documentation
is here @link[river-docs-url]{here}.

@;{
@section{API}

This API is a graphql API you can use to develop your own front end applications. The API
and schema defitition is available @link[river-api-url]{here}

@margin-note{The API is not stable and will most likely change frequetly.}
}

