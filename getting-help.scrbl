#lang scribble/manual

@title[#:tag "getting-help"]{Getting Help}

Questions, advice, suggestions, requests and everything else: @bold{
@link["mailto:incoming+csiro-crop-informatics/river-docs@incoming.gitlab.com"]{Email Us!}
}
