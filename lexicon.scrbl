#lang scribble/manual

@(require "graphviz.rkt")
@(require scriblib/figure)

@title{River Lexicon}

Talking about River can be challenging because of the the number
of related components. This article aims to reduce confusion by
defining a standard naming convention for all the artifiacts in
the River ecosystem. A key difficulty is distinguishing between
River itself and applications created using River. The distinction
may be further blurred as domain applications may share parts of 
the River instance.

@local-table-of-contents{}

@section{The River instance}

A River instance is an instantiation of the full River stack as 
described below. This includes the database, Prisma server, River
server, and River browser client.

@subsection{The River database}

The River database is the SQL database (Postgres or MySQL). When
speaking about River we make little distinction between the RDBMS
and the actual database. We are just talking about where the data
is stored. 

@subsection{The Prisma server}

The Prisma server is the @link["https://www.prisma.io/"]{Prisma server}.
It is a separate application that acts like a ORM by exposing a GraphQL
endpoint API. 

@subsection{The River server}

The River server exposes a GraphQL API similar to the Prisma server. Authentication
and Authorization run on the River server. The combination of the database,
Prisma server and River server can be thought of as the backend.

@subsection{The River browser client}

The River browser client refers to the combination of the ontology builder 
and the River default frontend. While these will both be present in all 
River instances, they might not be used in the domain applications. 

@subsubsection{The Ontology Builder}

@image["static/River-Ontology-Builder.png"]{Ontology Builder Image}
    
The ontology builder is the frontend widget for building onotologies. You can
get to the ontology build by clicking the wrench icon. The ontology builder is
where users can define their type system. 

@subsubsection{The River Default Frontend}

@image["static/River-Default-FE.png"]{Default Front End Image}

The Default Frontend is a default application for observation and measurement of
an ontology. Most applications will probably implement their own domain specific
frontend, but this default frontend will be available on all River instances.

@section{Domain (specific)? applications}

A domain application is an application that uses River as a backend to solve
a problem in a specific domain. There are two ways of creating a domain application
described below. 

@subsection{The forked application}

A forked application is an application that has been forked from River. Forked 
applications use River as a starting point to start building new domain
specific applications. While this method can be used to create applications
it is not recommended because a forked application will not receive updates
made to River and may not be able to integrate with other River applications.

@subsection{A frontend instance}

A frontend instance is an applications that uses a River instance as the backend. 
A frontend instance ignores the River default frontend and recreates it in a
domain friendly way.

@section{Types within River}

This next section looks at River at a slightly lower level. It details the components
that make up the database. We will use the following graphs as a demonstration to illustrate
how River types work. In this example we will model a chess tournment managment 
application.
 
@graphviz-figure["ontology-graph" "Chess Tournament Ontology Example"]{
  digraph G {
    Tournament [ fillcolor="red" style="filled" ]
    Game [ fillcolor="red" style="filled" ]
    Person [ fillcolor="red" style="filled" ]
    String [ fillcolor="green" style="filled" ]
    Number [ fillcolor="green" style="filled" ]

    Tournament -> Game [ label="games" ]
    Tournament -> Person [ label="players" ]
    Game -> Number [ label="game number" ]
    Game -> Person [ label="white player" ]
    Game -> Person [ label="black player" ]
    Tournament -> String [ label="name" ]
    Person -> String [ label="name" ]
    Game -> String [ label="result" ]
  }
}

@graphviz-figure["feature-graph" "Chess Tournament Feature Example"]{
  digraph G {
    "Tata Steel" [fillcolor="orange" style="filled"] 
    "US Open" [fillcolor="orange" style="filled"]
    "Candidates Tournament" [fillcolor="orange" style="filled"]

    "Magnus Carlsen" [fillcolor="cadetblue1" style="filled"]
    "Fabiano Caruana" [fillcolor="cadetblue1" style="filled"]
    "Vishy Anand" [fillcolor="cadetblue1" style="filled"]
    "Vlad Kramnik" [fillcolor="cadetblue1" style="filled"]

    "Game 1" [fillcolor="coral1" style="filled"]
    "Game 2" [fillcolor="coral1" style="filled"]

    "1 - 0" [fillcolor="deepskyblue1" style="filled"]
    "0 - 1" [fillcolor="deepskyblue1" style="filled"]
    
    "Candidates Tournament" -> "Fabiano Caruana"
    "Candidates Tournament" -> "Vishy Anand"
    "Candidates Tournament" -> "Vlad Kramnik"

    "US Open" -> "Fabiano Caruana"

    "Tata Steel" -> "Fabiano Caruana"
    "Tata Steel" -> "Magnus Carlsen"
    "Tata Steel" -> "Vlad Kramnik"
    "Tata Steel" -> "Vishy Anand"

    "Candidates Tournament" -> "Game 1"

    "Fabiano Caruana" -> "Game 1"
    "Vlad Kramnik" -> "Game 1"

    "Tata Steel" -> "Game 2"

    "Fabiano Caruana" -> "Game 2"
    "Vlad Kramnik" -> "Game 2"

    "Game 1" -> "0 - 1"
    "Game 2" -> "1 - 0"
  }
}

@subsection{Types}

A type is, well, a type of thing. It is a constructor for objects you would like to
create in the future. 

In the exampe @figure-ref["ontology-graph"] the nodes labeled in red are types. A 
type has a number of phenomenon which can contain further types or values.

@subsection{Features}

A feature is an instantiation of a type. @figure-ref["feature-graph"] shows a set
of features that may have been instantiated from the types in @figure-ref["ontology-graph"].
The tournaments are given in orange, the players in blue and the games in red.

@subsection{Phenomenon}

Phenomenon are things that can be observed on types. For example in @figure-ref["ontology-graph"]
@code|{"name"}| is a phenomenon on the @code|{"Person"}| type while @code|{"white player"}| is a phenomenon on the @code|{"Game"}|
type. Each arrow represents a phenomenon. A phenomenon can result in either a value literal 
(such as a number or label) or another type. 

@subsubsection{Type Phenomenon}

A type phenomenon is a phenomenon that results in another type. In @figure-ref["ontology-graph"]
the @code|{"games"}| phenomenon on the @code|{"Tournament"}| type is a type phenomenon because it resolves
to a new type, or in this case list of types.

@subsubsection{Literal Phenomenon}

A literal phenomenon is a phenomenon that will resolve to a literal value ie. a string
number, or list of numbers. In the example @figure-ref["ontology-graph"] the result 
phenomenon is literal because it resolves to a literal string value.

@subsection{Observation}

An observation is a phenomenon resolved on a feature. For instances the observations
@code|{"1 - 0"}| and @code|{"0 - 1"}| in @figure-ref["feature-graph"] resolve the @code|{"result"}| phenomenon
for the @code|{"game"}| features.

@subsection{Action}

Actions define pieces of logic that a user can define. This logic can interact with
any features or observations, but not types or phenomenon.

@margin-note{This feature is not implemented yet}

@subsection{Trigger}

A trigger describes when an action runs.

@margin-note{This feature is not implemented yet}

@subsubsection{User defined trigger}

When a user defined trigger is created a button will appear on the default frontend
page. Clicking this button will trigger the associated action.

@subsubsection{System defined trigger}

The system defined triggers are hooks that the system provides you to hook into
certain parts of the feature/phenomenon lifecycle. An example of a system trigger
may be @code|{"game.onUpdate"}|. 
