#lang scribble/manual

@title{The River Project}
@author[(author+email "Andrew Spriggs" "andrew.spriggs@csiro.au")]
@author[(author+email "Banner Schafer" "banner.schafer@csiro.au")]

Welcome to the documentation for the River project. Before reading the documentation
we should cover what exactly River is. While it is not difficult to list the features
and benefits of River, describing what it is in a concise matter is not easy. As 
River is designed to help a number of different roles within a project team, a succinct 
definition of the project is dependent on what role the reader has. The description 
assigned to River by each member of the team is significantly different as each individual
sees different features as the main benefit of the project. In the eyes of a developer,
River is an abstraction on observation & measurement applications allowing for rapid 
development of an ontology API. 

Depending on your skills and specialties you may find a better definition of the project
in the @secref["who-should-read-this"] this section. 

@table-of-contents{}

@include-section["who-should-read-this.scrbl"]
@include-section["contributing.scrbl"]
@include-section["background.scrbl"]
@include-section["lexicon.scrbl"]
@include-section["tech-stack.scrbl"]
@include-section["building-a-lexicon.scrbl"]
@include-section["using-a-river-app.scrbl"]
@include-section["getting-help.scrbl"]
@include-section["setting-up-a-development-version.scrbl"]
@include-section["development.scrbl"]
@include-section["standards.scrbl"]
