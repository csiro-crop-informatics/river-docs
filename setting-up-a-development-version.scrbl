#lang scribble/manual

@require["definitions.rkt"]

@title{Setting Up a Development Instance}

Like all software, there's more then one way to set up the project.
I will be recommending using docker compose to manage everything; 
however, I personally use docker compose only for the database and
Prisma server while running the browser client and River server
as separate services with npm. 

@local-table-of-contents{}

@section{Prerequisite}

At a minimum you will need to have the following software installed on 
your computer. This assumes you are using a relatively new version of 
Ubuntu.

@itemlist[
  @item{Docker and Docker Compose}
  @item{Git}
]

@section{Installing Docker}


@link["https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04"]{Install Docker Ubuntu 18.04}


@link["https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04"]{Install Docker Ubuntu 16.04}


@link["https://docs.docker.com/compose/install/"]{Install Docker Compose}

@section{Pulling the source}

Download the @link[river-repo]{source} repo.

@section{Running the services}

From the root of the river directory run.

@margin-note{That's not all! Keep reading till @secref["access"] to complete the setup.}

@codeblock|{
docker-compose up
}|

@section{Migrating the Database}

To migrate the database run

@codeblock|{
docker exec river_app-server_1 npm run prisma deploy
}|

@section[#:tag "access"]{Using the Application}

There will now be three services running.

@itemlist[
  @item{Prisma server: @link["http://127.0.0.1:4467"]{on port 4467}}
  @item{River server: @link["http://127.0.0.1:4000"]{on port 4000}}
  @item{Prisma browser client: @link["http://127.0.0.1:8080"]{on port 8080}}
]

@section{Running Migrations}

If you ever need to update your Prisma server schema file you can rerun.

@codeblock|{
docker exec river_app-server_1 npm run prisma deploy
}|

@section{Reseeding}

If you update the seeding scripts and would like to reseed you can run.

@codeblock|{
docker exec river_app-server_1 npm run -- prisma seed -r
}|

@section{Using Database}

To use mysql on the mysql database run.

@codeblock|{
docker exec -it river_mysql-db_1 mysql --database default@default -p
// password: prisma 
}|

@section{Notes on versions}

When you use docker compose you will not need to worry about versions of node
being used. If you decide to run your own versions of any applications please
develop using version 8 of node. 

