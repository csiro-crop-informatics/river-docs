#lang scribble/manual

@title{Standards}

The goal of this page is to describe a standard for programming on River.

@local-table-of-contents{}

@section{Coding Standards}

These standards were chosen pretty much arbitrarily. The point is not to make a 
statement about proper coding, but to provide a way to minimize git conflicts.

There are two rules:

@itemlist[
  @item{Follow the @link["https://prettier.io/"]{PrettierJs} standard. Configuration below.}
  @item{Use emoji in variable names sparingly}
]

@subsection{PrettierJs Configurations}

@codeblock|{
  {
    "print_width": 80,
    "tab_width": 2,
    "use_tabs": false,
    "semi": false, 
    "single_quote": true,
    "bracket_spacing": false,
    "jsx_bracket_same_line": true,
    "arrow_parens": "always",
    "trailing_comma": "all"
  }
}|

@section{Git Standards}

Use @link["https://datasift.github.io/gitflow/IntroducingGitFlow.html"]{Git Flow}.

