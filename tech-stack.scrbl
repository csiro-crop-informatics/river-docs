#lang scribble/manual


@title{The Technology Stack}

The goal of this page is to describe the technology stuff and enough 
information to get started working with the system. More detailed 
describptions of the technology are deferred to the tech's documentation.

@local-table-of-contents{}

@section{GraphQL}

GraphQL is a bit of a buzz word at the moment and as such there is a lot
of web content devouted to learning it. Most of this is garbage. I would
recommend the @hyperlink["https://graphql.org/learn/"]{official documentation}
if you are new to GraphQL. Also, be aware it does represent a massive change
in how we think about API's so delete what you know about REST. 

Here are a list of the GraphQL tools used in River.

@itemlist[
  @item{@link["https://github.com/prisma/graphql-yoga"]{GraphQL Yoga
  } - The equivalent of express.js for graphql}
  @item{@link["https://www.apollographql.com/docs/react"]{
    Apollo Client
  } - A client for querying GraphQL endpoints}
  @item{@link["https://www.prisma.io"]{
    Prisma
  } - GraphQL ORM layer}
  @item{@link["https://graphql.org/graphql-js"]{
    GraphQL JS library
  } - GraphQL schema builder for JavaScript}
]

@section{Prisma}

Prisma is mentioned in the section above, but deserves a section of its own.
Prisma is similar to an ORM in terms of functionality but the way it operates
is a little different. 

When starting a prisma app you define a set of graphql types and pass that
schema to Prisma. Prisma connects to a database, runs migrations to create
tables based on your schema, and runs a server with an @link["https://www.opencrud.org/"]{
open crud} api. 

You can then access this server from your api server. Most of what River does is 
authenticate and authorize a user and then proxy onto the Prisma server.

See Also: @hyperlink["https://www.prisma.io"]{Prisma}

@section{React}

React is another javascript framework for browser applications (there is a similar
project "react native" for native applications).

If you are unfamiliar with React I recommend @hyperlink["https://www.udemy.com/react-the-complete-guide-incl-redux/"]{this}
React tutorial. There is a heavy focus on redux as oppose to GraphQL, but it's
still worth a watch. 

See also: @hyperlink["https://reactjs.org/"]{React}

