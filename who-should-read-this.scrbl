#lang scribble/manual

@title[#:tag "who-should-read-this"]{Who Is This Documentation For?}

River is a software development tool for people who are not software developers.
A key idea behind River is that software developers should not be solely responsible
for developing software. That being said, don't worry if you are not a programmer
or don't know anything about programming. If you can use the internet you can 
build an application with River. 

River is designed to make teams function better by allowing each memeber of the
team to focus on their specialty. The domain experts do not have to write software,
the software developers do not need to write domain specific logic, the data 
professionals get their data processed the way they want it, and project managers
get all of their products to integrate seemlessly. If you start using River
you will get to focus on what you are an expert in. 

@local-table-of-contents{}

@section{River for Domain Experts}

If you are someone who defines how an application works you will no longer need to
describe your field to a software engineer to get an application built. You will
be able to build your ontology right into the application and
then start using it! You can describe how each entity in your field interacts with
other entities, events that happen and how they change entities, and the agents
that can trigger events and own entities. Once you have described your ontology
River will keep track of everything for you. As entities are created and destroyed
River will store everything, even when custom events happen you define how River
reacts. You're in control.

See Also: @secref["building-an-ontology"]

@section{River for Data Scientists}

You are not the end of the research process. You're where the learning starts. 
With River you will recieve data in the format you want, preprocessed based
on your own rules. You should not be spending time cleaning data. Datasets 
should be pre-cleaned and pre-processed ready for you to start building models.
Because of the consistent way in which River stores data, we can provide you
the data you want in the format you want. 

@section{River for Information Scientists}

We were frustrated by the number of implementations of PROV-O and O&M. With
River you can define a new model or use an existing model. 

See Also: @secref["building-an-ontology"]

@section{River for Software Developer}

You can stay focused on software engineering. When your specifications change,
your application does not have to. River decouples your business logic from your
application, meaning you can focus on application logic. 

@section{River for Product Owners & Project Managers}

River applications integrate really well with other River applications. That means
if you build your next two applications with River not only will development be
much faster, but you will be able to have your applications communicate simply. 
You won't need to coordinate multiple teams of engineers to build a new API
that both applications can share. 

@section{River for Users}

We want to make working with the application as easy as possible for you. We provide
an easy and intuitive interface for you to collect and view the information you need. 

See Also: @secref["using-river"], @secref["getting-help"]
